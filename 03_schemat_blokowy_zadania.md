# Schemat blokowy: zadania

Zadania, proszę wykonać w następujący sposób: [link](https://gitlab.com/sci_base/gitlab/-/blob/master/06_zadania.md)

Do rysowania proszę wykorzystać następujący program, który działa w przeglądarce [diragrams.net](https://app.diagrams.net/).

Aplikacja jest prosta w użyciu, ponieważ wystarczy zaznaczyć figurę z menu i przeciągnąć ją na arkusz.

### Figury do rysowania:
![](assets/diagrams.net_blocks.PNG)

### Rysowanie:
![](assets/diagrams.net_draw.gif)

### Łączenie:
![](assets/diagrams.net_connections.gif)

### Zapisywanie pliku:
![](assets/diagrams.net_save.gif)

## Zadania

```
Napisz  algorytm  za  pomocą schematów  blokowych:

- [ ] Zadanie_0: Obliczenie pola trójkąta prostokątnego.
- [ ] Zadanie_1: Sprawdzenie czy wczytana wartość x jest liczbą dodatnią, ujemną lub zerem.
- [ ] Zadanie_2: Obliczenie iloczynu n liczb.
- [ ] Zadanie_3: Kalkulator dwóch liczb (dodawanie, odejmowanie, mnożenie, dzielenie)
- [ ] Zadanie_4: Oblicanie NWD (największy wspólny dzielnik) i NWW (najmniejsza wspólna wielokrotność).
```

## Rozwiązanie
*Wszystkie rozwiązania proszę wrzucić w katalog o nazwie 03_schematblokowy.* \
Wynik rozwiązania jest obrazek wektorowy `svg`, który wrzucisz na swoje repozytorium za pomocą gita.
